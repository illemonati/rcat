use std::io;
use std::io::prelude::*;
use std::io::Write;

pub fn run_echo_mode() {
    for line in io::stdin().lock().lines() {
        let line = match line {
            Ok(line) => line,
            Err(_) => {
                return;
            }
        };
        let _ = io::stdout().write(line.as_bytes());
        let _ = io::stdout().write(b"\n");
        let _ = io::stdout().flush();
    }
}

