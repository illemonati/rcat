use std::io;
use std::fs;


pub fn run_file_mode(filepaths: &[String]) {
    let files = filepaths_to_files(filepaths);
    for mut file in files {
        write_file_to_stdout(&mut file);
    }
}


fn filepaths_to_files(filepaths: &[String]) -> Vec<fs::File> {
    let mut files = vec![];
    for filepath in filepaths {
        let file = fs::OpenOptions::new()
            .read(true)
            .write(true)
            .create(false)
            .open(filepath).expect(&format!("Error processing {}", filepath));
        files.push(file);
    }
    files
}

fn write_file_to_stdout(file: &mut fs::File) {
    let _ = io::copy(file, &mut io::stdout());
}