mod file_mode;
mod echo_mode;

use std::env;
use file_mode::run_file_mode;
use echo_mode::run_echo_mode;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        run_echo_mode();
        return;
    }
    let filepaths = &args[1..];
    run_file_mode(filepaths);
}





